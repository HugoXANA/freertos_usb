#ifndef USB_TASK_H
#define USB_TASK_H

#include "struct_typedef.h"

#define MSG_HEADER ((uint8_t)0x22)

typedef struct
{
	uint8_t state; // 0 = off, 1 = single shoot, 2 = continuous shoot
	uint8_t bullets_count; // number of bullets to shoot
	uint32_t frequency; // number of bullets to shoot in a second
} __attribute__((packed)) shoot_status_t;

typedef struct
{
	int32_t gimbol_yaw;
	int32_t gimbol_pitch;
	shoot_status_t shoot_status;
	uint8_t auto_status; //0 = manual, 1 = auto 
} __attribute__((packed)) outgoing_msg_t; //payload

typedef struct
{
	int32_t gimbol_yaw;
	int32_t gimbol_pitch;
	shoot_status_t shoot_status;
} __attribute__((packed)) incoming_msg_t; //payload

typedef struct
{
	incoming_msg_t gimbal_control_data;
	outgoing_msg_t gimbal_feedback_data;
	uint8_t USB_received;
	uint8_t usb_watch_dog; //count down counter
	int8_t msg_len;
	uint8_t receive_header;
	uint8_t receive_payload;
	uint8_t checksum_mismatch;
	uint8_t checksum_matches;
} USB_control_t;	//USB communication struct

void usb_task(void const * argument);
void usb_get_transmit_value(void);
uint8_t crc8(const void *vptr, int len);
bool USB_received_byte(USB_control_t *usb, uint8_t b);
// returval: whether received a complete packet, 0 = fail, 1 = succeed
bool usb_receive_byte(uint8_t data_byte); 
void usb_connection_judge(void);
void usb_instruction_handler(void);

extern USB_control_t USB_control;

#endif


